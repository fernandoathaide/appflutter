import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TransactionForm extends StatefulWidget {
  final void Function(String, double, DateTime) addTransaction;

  const TransactionForm(this.addTransaction, {super.key});

  @override
  State<TransactionForm> createState() => _TransactionFormState();
}

class _TransactionFormState extends State<TransactionForm> {
  final _titleController = TextEditingController();
  final _valueController = TextEditingController();
  DateTime? _selectedDate = DateTime.now();

  onSubmitForm() {
    final titleTexto = _titleController.text;
    var valueTexto = _valueController.text;

    if (valueTexto.contains(',')) {
      valueTexto = valueTexto.replaceAll(',', '.');
    }
    final valueDouble = double.tryParse(valueTexto) ?? 0.0;
    if (titleTexto.isEmpty || valueDouble <= 0) {
      return;
    }
    widget.addTransaction(titleTexto, valueDouble, _selectedDate!);
  }

  _showDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019),
      lastDate: DateTime.now(),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }

      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            TextField(
              controller: _titleController,
              onSubmitted: (_) => onSubmitForm(),
              decoration: const InputDecoration(
                icon: Icon(Icons.text_fields),
                labelText: 'Título:',
              ),
            ),
            TextField(
              controller: _valueController,
              decoration: const InputDecoration(
                icon: Icon(Icons.monetization_on),
                labelText: 'Valor (R\$) *:',
              ),
              keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
              onSubmitted: (_) => onSubmitForm(),
            ),
            SizedBox(
              height: 70,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      _selectedDate == null
                          ? 'Nenhuma data!'
                          : 'Data: ${DateFormat('dd/MM/y').format(_selectedDate!)}',
                    ),
                  ),
                  TextButton(
                    onPressed: _showDatePicker,
                    child: const Text(
                      'Selecionar Data',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                  onPressed: onSubmitForm,
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Theme.of(context).colorScheme.primary,
                    textStyle: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: Text('Nova Transação',
                      style: TextStyle(
                        color: Theme.of(context).textTheme.labelLarge?.color,
                      )),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
