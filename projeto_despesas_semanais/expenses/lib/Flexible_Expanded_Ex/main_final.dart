import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flexible & Expanded',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text('Flexible & Expanded'),
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            height: 100,
            child: const Text('Item 1 - pretty big!'),
            color: Colors.red,
          ),
          Expanded(
            flex: 4,
            child: Container(
              height: 100,
              child: const Text('Item 2'),
              color: Colors.blue,
            ),
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.loose,
            child: Container(
              height: 100,
              child: const Text('Item 3'),
              color: Colors.orange,
            ),
          ),
        ],
      ),
    );
  }
}



// Card(
//                   child: Row(
//                     children: <Widget>[
//                       Container(
//                         margin: const EdgeInsets.symmetric(
//                           horizontal: 15,
//                           vertical: 10,
//                         ),
//                         decoration: BoxDecoration(
//                           border: Border.all(
//                             color: Theme.of(context).colorScheme.primary,
//                             width: 2,
//                           ),
//                         ),
//                         padding: const EdgeInsets.all(10),
//                         child: Text(
//                           'R\$ ${tr.value.toStringAsFixed(2)}',
//                           style: TextStyle(
//                             fontWeight: FontWeight.bold,
//                             fontSize: 20,
//                             color: Theme.of(context).colorScheme.primary,
//                           ),
//                         ),
//                       ),
//                       Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         mainAxisSize: MainAxisSize.max,
//                         children: [
//                           Text(
//                             tr.title,
//                             style: Theme.of(context).textTheme.titleLarge,
//                             // const TextStyle(
//                             //   fontWeight: FontWeight.bold,
//                             //   fontSize: 16,
//                             // ),
//                           ),
//                           Text(
//                             DateFormat('d MMM y').format(tr.date),
//                             style: Theme.of(context).textTheme.titleSmall,
//                             // style: const TextStyle(
//                             //   fontSize: 16,
//                             //   color: Color.fromARGB(255, 107, 107, 107),
//                             // ),
//                           ),
//                         ],
//                       ),
//                       // const Column(
//                       //     crossAxisAlignment: CrossAxisAlignment
//                       //         .start, // Alinha os elementos à esquerda
//                       //     mainAxisAlignment: MainAxisAlignment
//                       //         .end, // Alinha a coluna no final do card
//                       //     mainAxisSize: MainAxisSize.max,
//                       //     children: [
//                       //       Text(
//                       //         'del', //ID
//                       //         style: TextStyle(
//                       //           fontWeight: FontWeight.bold,
//                       //           fontSize: 16,
//                       //         ),
//                       //       ),
//                       //       Text(
//                       //         'Alt', //ID
//                       //         style: TextStyle(
//                       //           fontWeight: FontWeight.bold,
//                       //           fontSize: 16,
//                       //         ),
//                       //       ),
//                       //     ])
//                     ],
//                   ),
//                 );

 // final List<Transaction> _transactions = [
  //   Transaction(
  //     id: 't0',
  //     title: 'Conta Antiga',
  //     value: 400.00,
  //     date: DateTime.now().subtract(const Duration(days: 33)),
  //   ),
  //   Transaction(
  //     id: 't1',
  //     title: 'Novo Tênis de Corrida',
  //     value: 310.76,
  //     date: DateTime.now().subtract(const Duration(days: 3)),
  //   ),
  //   Transaction(
  //     id: 't2',
  //     title: 'Conta de Luz',
  //     value: 211.30,
  //     date: DateTime.now().subtract(const Duration(days: 4)),
  //   ),
  //   Transaction(
  //     id: 't3',
  //     title: 'Cartão de Crédito',
  //     value: 100.30,
  //     date: DateTime.now(),
  //   ),
  //   Transaction(
  //     id: 't4',
  //     title: 'Lanche',
  //     value: 11.30,
  //     date: DateTime.now(),
  //   ),
  // ];