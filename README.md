# Comandos em Terminais
    Windows -> https://blog.cod3r.com.br/terminal-no-windows/
    Linux/Mac -> https://blog.cod3r.com.br/terminal-no-macos-e-linux/
    MELD app no Linux para comparar arquivos e pastas mostrar as diferenças
    Tilix + ZSH - Terminal para DEVOPS
    https://github.com/vaamonde/dell-linuxmint/blob/master/software/06-tilix.md

## Passo 01
_ Atualização do sistema utilizando o MintUpdate;
_ Atualização do sistema utilizando o Apt;

    Terminal: Ctrl + Alt + T
        sudo apt update
        sudo apt upgrade
        sudo apt full-upgrade
        sudo apt dist-upgrade
        sudo apt autoremove
        sudo apt autoclean

## Passo 02
    sudo apt install tilix unzip git vim python2 python3
## Passo 03
    #opção do comando mkdir: -v (verbose), -p (parents), ~ (til: alias home directory)
    #opção do comando fc-cache: -f (Force up-to-date cache files), -v (Display status)
    mkdir -pv ~/.local/share/fonts/Hack
    cd ~/.local/share/fonts/Hack
        wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Hack.zip
        unzip Hack.zip
        fc-cache -f -v
        exit
## Passo 04
    #OBSERVAÇÃO IMPORTANTE: no Linux Mint 20.x a mudança do Terminal padrão é feita utilizando
    o recurso das Configurações do Sistema em Aplicativos Preferenciais
    Menu
        Aplicativos Preferenciais
            Terminal
                Tilix

    #OBSERVAÇÃO IMPORTANTE: no Linux Mint 21.x a mudança do Terminal padrão não é mais feita
    utilizando o recurso das Configurações do Sistema em Aplicativos Preferenciais, essa opção
    foi retirada da ferramenta, sendo necessário alterar via linha de comando:

    #opção do comando gsettings: set (Sets the value of KEY to VALUE)
    gsettings set org.cinnamon.desktop.default-applications.terminal exec /usr/bin/tilix
    exit
## Passo 05
    Atalho: Ctrl + Alt + T
## Passo 06
    Menu
        Preferências
            Aparência
                Usar abas em vez da barra lateral (necessário reiniciar aplicação)
            Padrão
                Geral
                    Fonte Personalizada: Yes
                        Hack Nerd Font Mono Bold
                        Tamanho: 16
                Cor
                    Esquemas de Cores: Monokai Dark
                    Transparência: Aumentar um Pouco
    Fechar
## Passo 07
    Terminal: Ctrl + Alt + T

    sudo apt install zsh
    zsh
        Type one of the keys in parentheses: 2

    #opção do comando sh: -c (Read commands from the command_string operand in‐stead of from the standard input)
    #opções do comando curl: -f (fail), -s (silent), -S (show-error), -L (location) 
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        Do you want to change your default shell to zsh? [Y/n] Y <Enter>
        [sudo] senha do seu usuário: <Enter>

    #recomendo fechar o Terminal Tilix e abrir novamente para verificar se tudo está funcionando
    #será necessário sair 03 (três) vezes do terminal para concluir a configuração
    exit
    exit
    exit
## Passo 08
    Atalho: Ctrl + Alt + T

    Link de referência: https://github.com/zsh-users/zsh-syntax-highlighting
    Link de referência FZF: https://github.com/junegunn/fzf.git

    #Instalação do Plugin Highlighting do ZSH
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

    #Instalação do Plugin Zsh-AutoSuggestions do ZSH
    git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions

    #Instalação do Plugin K do ZSH
    git clone https://github.com/supercrabtree/k $ZSH_CUSTOM/plugins/k

    #Instalação do Tema PowerLevel10K
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel10k

    #Instalação do Plugin FZF do ZSH
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install
        Do you want to enable fuzzy auto-completion? ([y]/n) y <Enter>
        Do you want to enable key bindings? ([y]/n) y <Enter>
        Do you want to update your shell configuration files? ([y]/n) y <Enter>
## Passo 09
    vim ~/.zshrc
        INSERT

            #Variáveis de configuração do Tema PowerLevel10K
            #Copiar e colar substituindo a partir da linha: 11
            ZSH_THEME="powerlevel10k/powerlevel10k"
            POWERLEVEL10K_MODE="nerdfont-complete"

            #Variável de configuração dos Plugins do ZSH
            #Copiar e colar substituindo a partir da linha: 75
            plugins=(
                git
                zsh-syntax-highlighting
                fzf
                zsh-autosuggestions
                k
            )

        ESC SHIFT :x <Enter>

    #recomendo fechar o Terminal Tilix e abrir novamente para verificar se tudo está funcionando,
    #deverá iniciar automaticamente a customização do Tema PowerLevel10K utilizando o Wizard padrão.
## Passo 10
    #abrir um novo terminal para iniciar as configurações do PowerLevel10K
    Terminal: Ctrl + Alt + T

        Does this look like a diamond (rotated square)?
            Choice [ynq]: y
        
        Does this look like a lock?
            Choice [ynq]: y
        
        Does this look like a Debian logo (swirl/spiral)?
            Choice [ynq]: y
        
        Do all these icons fit between the crosses?
            Choice [ynq]: y
        
        Prompt Style: (2) Classic
            Choice [1234rq]: 2
        
        Character Set: (1) Unicode
            Choice [1234rq]: 1
        
        Prompt Color: (4) Darkest.
            Choice [1234rq]: 4
        
        Show current time?: (2) 24-hour format.
            Choice [123rq]: 2
        
        Prompt Separators: (3)  Slanted.
            Choice [123rq]: 3
        
        Prompt Heads: (1)  Sharp.
            Choice [123rq]: 1
        
        Prompt Tails: (1)  Flat.
            Choice [123rq]: 1
        
        Prompt Height: (2)  Two lines.
            Choice [123rq]: 2
        
        Prompt Connection: (1)  Disconnected.
            Choice [123rq]: 1
        
        Prompt Frame: (1)  No frame.
            Choice [123rq]: 1
        
        Prompt Spacing: (2)  Sparse.
            Choice [123rq]: 2
        
        Icons: (2)  Many icons.
            Choice [123rq]: 2
        
        Prompt Flow: (2)  Fluent.
            Choice [123rq]: 2
        
        Enable Transient Prompt? (n)  No.
            Choice [n]: n
        
        Instant Prompt Mode: (1)  Verbose (recommended).
            Choice [123rq]: 1
        
        Apply changes to ~/.zshrc?: (y)  Yes (recommended).
            Choice [ynrq]: y

    #caso queira reconfigurar o PowerLevel10K novamente digite o comando abaixo.
    p10k configure
## Passo 11
    OBSERVAÇÃO IMPORTANTE: executar esse procedimento somente se você tem instalado
    o Microsoft Visual Studio no seu Linux Mint

    Gerenciar
        Configurações
            terminal.integrated.fontFamily
                Hack Nerd Font

    Ctrl + Shift + P
        Terminal: Selecionar o Perfil Padrão
            zsh

# appFlutter
	flutter run
	flutter devices
	flutter clean
	flutter run --enable-software-rendering
	flutter pub get //Faz o Download do que precisa de dependências



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/fernandoathaide/appflutter.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/fernandoathaide/appflutter/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
