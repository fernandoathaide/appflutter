import 'package:flutter/material.dart';

class Resultado extends StatelessWidget {
  final int pontuacao;
  final void Function() reiniciarQuestionario;

  const Resultado(this.pontuacao, this.reiniciarQuestionario, {super.key});

  String get fraseResultado {
    String texto;
    if (pontuacao < 8) texto = 'Parabéns!';
    if (pontuacao < 12) texto = 'Você é Bom!';
    (pontuacao < 16) ? texto = 'Impressionante!' : texto = 'Nível Jedi!';
    return '$texto \n Pontuação total ${pontuacao.toString()} ';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Text(
            fraseResultado,
            style: const TextStyle(fontSize: 28),
            textAlign: TextAlign.center,
          ),
        ),
        ElevatedButton(
          onPressed: reiniciarQuestionario,
          style: ElevatedButton.styleFrom(
            backgroundColor: const Color.fromARGB(
                255, 136, 12, 3), // Define a cor de fundo do botão
            foregroundColor: Colors.white, // Define a cor do texto do botão
            textStyle:
                const TextStyle(fontSize: 20), // Define o estilo do texto
            padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 10), // Define o espaçamento interno do botão
          ),
          child: const Text('<< Voltar'),
        ),
      ],
    );
  }
}
