import 'package:flutter/material.dart';
import './resultado.dart';
import './questionario.dart';

//CLASS MAIN PRINCIPAL START DO PROJETO
void main() => runApp(const PerguntaApp());

//CLASS STATEFUL PRINCIPAL (CLASS STATICS)
class PerguntaApp extends StatefulWidget {
  const PerguntaApp({super.key});

  @override
  PerguntaAppState createState() {
    return PerguntaAppState();
  }
}

//CLASS DINÂMICA STATE QUE TORNA A CLASS STATEFUL DINÂMICA
class PerguntaAppState extends State<PerguntaApp> {
  int _perguntaSelecionada = 0;
  int _pontuacaoTotal = 0;
  final List<Map<String, Object>> _perguntas = const [
    {
      'texto': 'Qual é a sua cor favorita?',
      'respostas': [
        {'texto': 'Azul', 'ponto': 10},
        {'texto': 'Verde', 'ponto': 5},
        {'texto': 'Vermelho', 'ponto': 1},
      ],
    },
    {
      'texto': 'Qual é o seu animal favorito?',
      'respostas': [
        {'texto': 'Gato', 'ponto': 3},
        {'texto': 'Cão', 'ponto': 6},
        {'texto': 'Pato', 'ponto': 5},
      ],
    },
    {
      'texto': 'Qual é o seu instrumento Favorito?',
      'respostas': [
        {'texto': 'Piano', 'ponto': 4},
        {'texto': 'Cavaco', 'ponto': 5},
        {'texto': 'Violão', 'ponto': 8},
      ],
    }
  ];

  void _responder(int pontucao) {
    if (temPerguntaSelecionada) {
      setState(() {
        _perguntaSelecionada++;
        _pontuacaoTotal += pontucao;
      });
    }
    //print(_pontuacaoTotal);
  }

  void _reiniciarQuestionario() {
    setState(() {
      _perguntaSelecionada = 0;
      _pontuacaoTotal = 0;
    });
  }

  bool get temPerguntaSelecionada {
    return _perguntaSelecionada < _perguntas.length;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Perguntas'),
        ),
        body: temPerguntaSelecionada
            ? Questionario(
                perguntas: _perguntas,
                perguntaSelecionada: _perguntaSelecionada,
                quandoResponder: _responder,
              )
            : Resultado(_pontuacaoTotal, _reiniciarQuestionario),
      ),
    );
  }
}
