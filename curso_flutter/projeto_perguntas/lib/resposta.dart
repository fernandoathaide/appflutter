import 'package:flutter/material.dart';

class Resposta extends StatelessWidget {
  final String resposta;
  final void Function() onSelecao;

  const Resposta(this.resposta, this.onSelecao, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.fromLTRB(5, 10, 5, 10),
      child: ElevatedButton(
        onPressed: onSelecao,
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.blue, // Define a cor de fundo do botão
          foregroundColor: Colors.white, // Define a cor do texto do botão
          textStyle: const TextStyle(fontSize: 20), // Define o estilo do texto
          padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 10), // Define o espaçamento interno do botão
        ),
        child: Text(resposta),
      ),
    );
  }
}
